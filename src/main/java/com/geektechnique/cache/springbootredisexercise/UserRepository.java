package com.geektechnique.cache.springbootredisexercise;

import com.geektechnique.cache.springbootredisexercise.model.User;

import java.util.Map;


//Why is there no @Repository or some type of Stereotype annotation here? How does this get picked up into the app context via component scanning?

//because UserRepositoryImpl imports and implements this class
public interface UserRepository {
    void save(User user);
    Map<String, User> findAll();
    User findById(String id);
    void update(User user);
    void delete(String id);
}
